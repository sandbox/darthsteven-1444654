<?php

class FeedsDOIssueParser extends FeedsParser {

  /**
   * Implements FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $raw = trim($fetcher_result->getRaw());
    if (empty($raw)) {
      throw new Exception(t('FeedsDOIssueParser parser: The document is empty.'));
    }

    $opts = array('ignore_parser_warnings' => TRUE);
    $result = new FeedsParserResult();
    // Set link so we can set the result link attribute.
    $fetcher_config = $source->getConfigFor($source->importer->fetcher);
    $result->link = $fetcher_config['source'];

    $doc = @qp($raw, NULL, $opts);

    // Convert document to UTF-8
    $ContentType = qp($doc, 'meta[http-equiv="content-type"]');
    if ($ContentType->hasAttr('content') && preg_match('/charset=([-\w]*)/i', $ContentType->attr('content'), $matches)) {
      $ContentType->attr('content', preg_replace('/charset=([-\w]*)/i', 'charset=utf-8', $ContentType->attr('content')));
      qp($doc, 'meta[http-equiv="content-type"]')->remove();
      qp($doc, 'head')->prepend($ContentType->html());
      $doc = qp(drupal_convert_to_utf8(utf8_decode($doc->html()), $matches[1]), NULL, $opts);
    }

    $parsed_item = array();

    // Get the title.
    $parsed_item['issue_title'] = qp($doc, '#page-subtitle', $opts)->text();
    //$result->title = $parsed_item['issue_title'];

    // Now find the submitted info and extract the created date and nid.
    $node_div_id = qp($doc, 'div.node-type-project_issue', $opts)->attr('id');
    $parsed_item['issue_nid'] = str_replace('node-', '', $node_div_id);
    $parsed_item['issue_url'] = 'http://drupal.org/node/' . $parsed_item['issue_nid'];

    $date_str = qp($doc, 'div.node-type-project_issue div.submitted em', $opts)->text();
    $parsed_item['issue_created_date'] = str_replace(' at', '', $date_str);

    $issue_summary_table = qp($doc, '#project-issue-summary-table tbody tr', $opts);

    foreach ($issue_summary_table as $row) {
      $label = qp($row, 'td:first', $opts)->text();
      $value = qp($row, 'td:last', $opts)->text();

      $label = drupal_strtolower(rtrim($label, ':'));

      switch ($label) {
        case 'project':
        case 'version':
        case 'component':
        case 'category':
        case 'priority':
        case 'assigned':
        case 'status':
          $parsed_item['issue_' .  str_replace(' ', '_', $label)] = $value;
          break;
        case 'issue tags':
          $parsed_item['issue_tags'] = explode(', ', $value);
          break;
      }
    }
    $result->items[] = $parsed_item;


    return $result;
  }

  public function getMappingSources() {
    $sources = parent::getMappingSources();

    $sources['issue_nid'] = t('Issue nid');
    $sources['issue_created_date'] = t('Issue created date');
    $sources['issue_title'] = t('Issue title');
    $sources['issue_url'] = t('Issue url');

    $sources['issue_project'] = t('Issue project');
    $sources['issue_version'] = t('Issue version');
    $sources['issue_component'] = t('Issue component');
    $sources['issue_category'] = t('Issue category');
    $sources['issue_priority'] = t('Issue priority');
    $sources['issue_assigned'] = t('Issue assignee');
    $sources['issue_status'] = t('Issue status');
    $sources['issue_tags'] = t('Issue tags');


    return $sources;
  }

}
